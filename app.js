const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const productRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');

//mongoose.connect('http://localhost:5222/',);
mongoose.connect("mongodb+srv://node-test:node-test@node-rest-test-5oj1o.mongodb.net/test?retryWrites=true", { useNewUrlParser: true });

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
	res.header('Access-control-allow-origin', '*');
	res.header('Access-control-allow-Header', 'Origin, X-Requested-With, Content-Type, Accept, Authorizaion');


if (req.method == 'OPTIONS') {
	res.header('Access-Control-Allow-Method', 'PUT, POST, PATCH, DELETE, GET');
	return res.status(200).json({});
}

next();	
});





// Routs which should start server 
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

app.use((req, res, next) => {
	const error = new Error('Not found');
	error.status = 404;
	next(error);
})

app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
		error: {
			message: error.message
		}
	});

});

module.exports = app;

